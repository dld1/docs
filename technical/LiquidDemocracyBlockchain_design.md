# Liquid Democracy Blockchain Technical Design
The Liquid Democracy Blockchain consists of voter registrars, voters, tabulators, and a git repository containing the current state of legislation for the electorate represented by the voters in this chain.
## Security
The Liquid Democracy Blockchain will use [PGP Encryption](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) to digitally sign each block. The chain itself does not need to be encrypted since most uses of DLD will require the data stored in the chain to be accessible to all for verification purposes, though it could be encrypted if required. All keys added to the chain must not be accepted if they are less than 128 bits in the symmetric equivalent.
## Blocks
### Genesis
The genesis block is the starting point for any liquid democracy blockchain. It contains the genesis super registrar's public key and a link to the current location of the legislation repository.

The genesis super registrar cannot be removed from the blockchain without starting a new chain so it is imperative that their private key remain secret.
#### Structure
```
{
  "type": "GENESIS",
  "superRegistrarId": "Some Unique External Identifier",
  "superRegistrarPublicKey": "Generated GPG Public Key",
  "legislationRepositorySourceURI": "https://some.git.repo/repo.git",
  "legislationRepositoryMainlineBranchName": "main",
  "legislationRepositoryMainlineHeadHash": "0123456789abcdef...",
  "specVersion": "1",
  "gpgSignature": "GPG Signature of block fields by super registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any block fields are missing

  fail if not able to clone b.legislationRepositorySourceURI

  fail if not able to fetch origin/${b.legislationRepositoryMainlineBranchName}

  fail if hash of HEAD commit of origin/${b.legislationRepositoryMainlineBranchName} does not match b.legislationRepositoryMainlineHeadHash
  
  fail if signature of fields does not match block signature
```
### Create Registrar
Super registrars may add voter registrars to the block chain who may also optionally be super registrars. Only the genesis super registrar can add and edit super registrars.

Registrars are responsible for maintaining voter and tabulator records in the blockchain.

#### Structure
```
{
  "type": "CREATE_REGISTRAR",
  "superRegistrarId": "Registering Super Registrar Identifier",
  "registrarId": "New Registrar Unique Identifier",
  "registrarPublicKey": "Generated GPG Public Key",
  "isSuperRegistrar": boolean,
  "expires": <Nullable Date>,
  "gpgSignature": "GPG Signature of block fields by registering super registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any block fields are missing
  
  superRegistrarRecord <- determine the most recent record for the super registrar associated with b.superRegistrarId

  fail if superRegistrarRecord not found
  
  fail if superRegistrarRecord.expires is not null AND superRegistrarRecord.expires < current time
  
  if b.isSuperRegistrar AND NOT superRegistrarRecord.isGenesisSuperRegistrar
    fail
  
  fail if signature of fields does not match block signature according to superRegistrarRecord.publicKey
```
### Update Registrar
Changes one or more attributes of a registrar. May only be done by a super registrar.

#### Structure
```
{
  "type": "UPDATE_REGISTRAR",
  "superRegistrarId": "Registering Super Registrar Identifier",
  "registrarId": "Registrar Unique Identifier",
  "isSuperRegistrar": boolean,
  "expires": <Nullable Date>,
  "gpgSignature": "GPG Signature of block fields by registering super registrar private key"
}
```

#### Validation
```
def validateBlock(Block b):
  fail if any of the following are missing:
    * superRegistrarId
    * registrarId
    * gpgSignature

  registrarRecord <- determine the most recent record for the registrar associated with b.registrarId

  fail if registrarRecord not found
  
  superRegistrarRecord <- determine the most recent record for the super registrar associated with b.superRegistrarId

  fail if superRegistrarRecord not found
  
  fail if superRegistrarRecord.expires is not null AND superRegistrarRecord.expires < current time

  if b.isSuperRegistrar AND NOT registrarRecord.isSuperRegistrar AND NOT superRegistrarRecord.isGenesisSuperRegistrar
    fail
  
  fail if signature of fields does not match block signature according to superRegistrarRecord.publicKey
```
### Create Voter
A registrar adds a new voter to the chain.
#### Structure
```
{
  "type": "CREATE_VOTER",
  "registrarId": "Registering Voter Registrar Identifier",
  "voterId": "External System Voter Identifier",
  "voterPublicKey": "Voter public key",
  "expires": <non-nullable future date>,
  "gpgSignature": "GPG Signature of block fields by registering registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any block fields are missing
  
  registrarRecord <- determine the most recent record for the registrar associated with b.registrarId

  fail if registrarRecord not found
  
  fail if registrarRecord.expires is not null AND registrarRecord.expires < current time
  
  fail if b.expires is < current time

  fail if signature of fields does not match block signature according to registrarRecord.publicKey
```
### Update Voter
A registrar updates a voter record in the chain.
#### Structure
```
{
  "type": "UPDATE_VOTER",
  "registrarId": "Registering Voter Registrar Identifier",
  "voterId": "External System Voter Identifier",
  "voterPublicKey": "Voter public key",
  "expires": <non-nullable future date>,
  "gpgSignature": "GPG Signature of block fields by registering registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
 fail if any of the following are missing:
    * registrarId
    * voterId
    * expires
    * gpgSignature
  
  registrarRecord <- determine the most recent record for the registrar associated with b.registrarId

  fail if registrarRecord not found
  
  fail if registrarRecord.expires is not null AND registrarRecord.expires < current time
  
  fail if signature of fields does not match block signature according to registrarRecord.publicKey
```
### Create Tabulator
A registrar adds a new tabulator to the chain.
#### Structure
```
{
  "type": "CREATE_TABULATOR",
  "registrarId": "Registering Voter Registrar Identifier",
  "tabulatorId": "External System Tabulator Identifier",
  "tabulatorPublicKey": "Tabulator public key",
  "expires": <nullable future date>,
  "gpgSignature": "GPG Signature of block fields by registering registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any of the following are missing:
    * type
    * registrarId
    * tabulatorId
    * tabulatorPublicKey
    * gpgSignature
  
  registrarRecord <- determine the most recent record for the registrar associated with b.registrarId

  fail if registrarRecord not found
  
  fail if registrarRecord.expires is not null AND registrarRecord.expires < current time
  
  fail if b.expires is not null and is < current time

  fail if signature of fields does not match block signature according to registrarRecord.publicKey
```
### Update Tabulator
A registrar updates a tabulator record in the chain.
#### Structure
```
{
  "type": "UPDATE_TABULATOR",
  "registrarId": "Registering Voter Registrar Identifier",
  "tabulatorId": "External System Tabulator Identifier",
  "tabulatorPublicKey": "Tabulator public key",
  "expires": <non-nullable future date>,
  "gpgSignature": "GPG Signature of block fields by registering registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
 fail if any of the following are missing:
    * registrarId
    * tabulatorId
    * gpgSignature
  
  registrarRecord <- determine the most recent record for the registrar associated with b.registrarId

  fail if registrarRecord not found
  
  fail if registrarRecord.expires is not null AND registrarRecord.expires < current time
  
  fail if signature of fields does not match block signature according to registrarRecord.publicKey
```
### Propose Legislation
A voter proposes changes to the legislation repository.
#### Structure
```
{
  "type": "PROPOSE_LEGISLATION",
  "voterId": "Some Unique External Identifier",
  "legislationProposalSourceURI": "https://some.git.repo/repo.git",
  "legislationProposalBranchName": "proposed-branch",
  "legislationProposalHeadHash": "0123456789abcdef...",
  "voteOpenTime": "2020-10-11T00:00:00",
  "voteCloseTime": "2020-10-18T00:00:00",
  "gpgSignature": "GPG Signature of block fields by voter private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any block fields are missing

  fail if b.voteOpenTime is after b.voteCloseTime

  voterRecord <- determine the most recent record for the voter associated with b.voterId

  fail if voterRecord not found OR voterRecord.expires < current time

  fail if not able to set b.legislationRepositorySourceURI as a remote

  fail if not able to fetch <b.legislationRepositorySourceURI remote name>/${b.legislationRepositoryProposalBranchName}

  fail if hash of HEAD commit of origin/${b.legislationRepositoryProposalBranchName} does not match b.legislationRepositoryProposalHeadHash

  potentiallyConflictingLegislation <- retrieve all blocks with type = "PROPOSE_LEGISLATION" and voteCloseTime is after current time and voteOpenTime is before b.voteCloseTime and voteCloseTime is not after b.voteCloseTime

  for each proposal in potentiallyConflictingLegislation:
    fail if b.legislationProposalBranchName cannot cleanly rebase off proposal.legislationProposalBranchName
  
  fail if signature of fields does not match block signature
```
### Delegate Vote
A voter delegates their vote to another voter. The delegation may be unconstrained or constrained to a single issue. Delegation may be overridden by a simple direct user vote on a particular issue.
#### Structure
```
{
  "type": "DELEGATE_VOTE",
  "delegatorId": "Some Unique External Identifier",
  "delegateId": "Some Other Unique External Identifier",
  "proposalSignature": "Optional GPG Signature of a proposal in the chain"
  "delegationEndTime": "2021-10-18T00:00:00",
  "gpgSignature": "GPG Signature of block fields by delegator private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any of the following are missing:
    * type
    * delegatorId
    * delegateId
    * delegationEndTime
    * gpgSignature
  
  delegatorRecord <- determine the most recent record for the voter associated with b.delegatorId

  fail if delegatorRecord not found OR delegatorRecord.expires < current time

  delegateRecord <- determine the most recent record for the voter associated with b.delegateId

  fail if delegateRecord not found OR delegateRecord.expires < current time

  fail if b.delegationEndTime < current time

  if b.proposalSignature is not null:
    proposalRecord <- find the proposal record in the chain

    fail if proposalRecord not found OR proposalRecord.voteCloseTime < current time

  fail if signature of fields does not match block signature according to delegatorRecord.publicKey
```
### Vote
A voter votes on a proposal. The voter must be eligible to vote at the proposal's voteOpenTime and the vote must be received no later than the proposal's voteCloseTime.
#### Structure
```
{
  "type": "VOTE",
  "voterId": "Some Unique External Identifier",
  "proposalSignature": "GPG Signature of a proposal in the chain"
  "vote": true/false, // Boolean value representing Yes, No
  "gpgSignature": "GPG Signature of block fields by voter private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any of block fields are missing

  voterRecord <- determine the most recent record for the voter associated with b.voterId

  proposalRecord <- find the proposal record in the chain

  fail if proposalRecord not found OR proposalRecord.voteOpenTime > current time OR proposalRecord.voteCloseTime < current time

  fail if voterRecord.expires < proposalRecord.voteOpenTime

  fail if signature of fields does not match block signature according to voterRecord.publicKey
```
### Vote Tally
Tally the votes on an issue after the vote close time has passed. Tabulators will each write their results to the chain independently and once a concensus can be established the vote result will be published.

Validation for a vote tally block will be basic to avoid forcing nodes to do a full computation to verify accuracy. To ensure the integrity of the voting system tabulators should be regularly audited to ensure compliance with the tabulation algorithm and violators must be removed from the chain.
#### Structure
```
{
  "type": "VOTE_TALLY",
  "tabulatorId": "Some Unique External Identifier",
  "proposalSignature": "GPG Signature of a proposal in the chain",
  "totalVotes": 10,
  "totalDirectVotes": 7,
  "totalYea": 6,
  "totalNay": 4,
  "resolutionPassed": true/false,
  "yeaChecksum": "aaaabbbbccccddddeeeeffff1234567890",
  "nayChecksum": "0000111122223333444455556789abcdef",
  "gpgSignature": "GPG Signature of block fields by tabulator private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any of block fields are missing or have incorrect data types (e.g. non-integer in an integer field)

  numDirectVotes <- count of the number of direct votes on the issue at closing time

  fail if b.totalDirectVotes != numDirectVotes

  tabulatorRecord <- determine the most recent record for the tabulator associated with b.tabulatorId

  fail if tabulatorRecord not found OR tabulatorRecord.expires < current time

  proposalRecord <- find the proposal record associated with b.proposalSignature

  fail if proposalRecord not found OR proposalRecord.voteCloseTime > current time

  fail if signature of fields does not match block signature according to tabulatorRecord.publicKey
```
#### Tabulation Algorithm
```
def tabulate(Blockchain blockChain, ProposalBlock b):
  fail if b.voteCloseTime > current time

  tallyBlock <- {}

  allDirectVotes <- blockChain.blocks
                        .filter(block -> block.type = "VOTE")
                        .filter(block -> block.proposalSignature = b.gpgSignature)
                        .collectAsList()

  tallyBlock.totalDirectVotes <- allDirectVotes.size

  voteMap = {}
  for each voteBlock in allDirectVotes:
    recurseDelegators(voteBlock, b, blockChain, voteMap))

  tallyBlock.totalVotes <- voteMap.size
  totalYea <- 0
  yeaChecksum <- 0
  totalNay <- 0
  nayChecksum <- 0
  for each voterId, vote in voteMap:
    # Hash function and modulo value here TBD
    if vote:
      totalYea++
      yeaChecksum <- (yeaChecksum + hash(voterId)) % someModulus
    else:
      totalNay++
      nayChecksum <- (nayChecksum + hash(voterId)) % someModulus

  tallyBlock.totalYea <- totalYea
  tallyBlock.yeaChecksum <- yeaChecksum
  tallyBlock.totalNay <- totalNay
  tallyBlock.nayChecksum <- nayChecksum

  tallyBlock.resolutionPassed <- totalYea > totalNay
  return tallyBlock

def recurseDelegators(VoteBlock voteBlock, ProposalBlock proposalBlock, Blockchain blockChain, Map voteMap):
  voterBlock <- find latest voter block in blockChain associated with voteBlock.voterId

  if voterBlock not found OR voterBlock.expires is before proposalBlock.voteOpenTime:
    return
  
  if voteMap[voteBlock.voterId] == null:
    voteMap[voteBlock.voterId] = voteBlock.vote
  
  proposalDelegateOverrides <- blockChain.blocks
                        .filter(block -> block.type = "DELEGATE_VOTE")
                        .filter(block -> block.delegateId != voteBlock.voterId)
                        .filter(block -> block.proposalSignature = proposalBlock.gpgSignature)
                        .collectAsList();

  proposalDelegators <- blockChain.blocks
                        .filter(block -> block.type = "DELEGATE_VOTE")
                        .filter(block -> block.delegateId = voteBlock.voterId)
                        .filter(block -> block.proposalSignature = proposalBlock.gpgSignature)
                        .collectAsList();

  generalDelegators <- blockChain.blocks
                        .filter(block -> block.type = "DELEGATE_VOTE")
                        .filter(block -> block.delegateId = voteBlock.voterId)
                        .filter(block -> block.proposalSignature is null)
                        .filter(block -> proposalDelegateOverrides does not contain block.delegatorId)
                        .collectAsList();

  delgators <- proposalDelegators + generalDelegators

  for voteDelegationBlock in delegators:
    recurseDelegators({voterId: voteDelegationBlock.delegatorId, vote: voteBlock.vote}, proposalBlock, blockChain, voteMap)

  return
```
### Vote Result
Once a sufficient number of tabulators (i.e. 50% + 1) agree on the total, a result block is published which will then trigger integration of the proposal branch to the mainline if it was a passing vote.
#### Structure
```
{
  "type": "VOTE_RESULT",
  "tabulatorId": "Some Unique External Identifier",
  "tabulatorConsensus": [
    "Some Unique External Identifier A",
    "Some Unique External Identifier B"
  ],
  "resolutionPassed": true/false,
  "proposalSignature": "GPG Signature of a Proposal in the Chain",
  "gpgSignature": "GPG Signature of block fields by tabulator private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any of block fields are missing

  fail if b.totalDirectVotes != number of unique voters who voted on this issue

  tabulatorRecord <- determine the most recent record for the tabulator associated with b.tabulatorId

  fail if tabulatorRecord not found OR tabulatorRecord.expires < current time

  for each record in b.tabulatorConcensus:
    validate record and fail if invalid or not found
    fail if record.resolutionPassed != b.resolutionPassed

  fail if signature of fields does not match block signature according to tabulatorRecord.publicKey
```
### Update Spec Version
Updates the version of the DLD spec that the chain implements from this point on.
#### Structure
```
{
  "type": "UPDATE_SPEC",
  "specVersion": "2",
  "gpgSignature": "GPG Signature of block fields by genesis super registrar private key"
}
```
#### Validation
```
def validateBlock(Block b):
  fail if any of the block fields are missing

  fail if signature of fields does not match block signature according to the genesis  super registrar's public key
```
## Synchronization
The DLD chain will use a longest valid chain strategy when synchronizing with other nodes.

It is unlikely but possible that a shorter-chained node who had a previously passing proposal that been integrated in its git repository would have the proposal reversed if it accepted a longer chain of voters that subsequently changed the tally on that proposal. If this happens, the node must accept the override from the incoming longer chain.